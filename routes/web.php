<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\EmployerController;
use App\Http\Controllers\InventoryController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\PromotionController;
use App\Http\Controllers\SaleController;
use App\Models\Customer;
use App\Models\Inventory;
use App\Models\Promotion;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/customersprofile',                   [CustomerController::class, 'viewCustomerProfile']);
Route::get('/customers/profile/create',           [CustomerController::class, 'create']);
Route::post('/customers/profile/form',            [CustomerController::class, 'insert']);
Route::get('/customers/profile/edit/{id}',        [CustomerController::class, 'edit']);
Route::put('/customers/profile/form',             [CustomerController::class, 'update']);
Route::get('/customers/profile/edit/delete/{id}', [CustomerController::class, 'destroy']);

Route::get('/employees/profile',                  [EmployerController::class, 'viewEmployerProfile']);
Route::get('/employees/profile/delete/{id}',      [EmployerController::class, 'destroy']);
Route::get('/employees/profile/edit/{id}',        [EmployerController::class, 'edit']);
Route::get('/employees/profile/create',           [EmployerController::class, 'create']);
Route::get('/employees/profile',                  [EmployerController::class, 'viewEmployerProfile']);
Route::put('/employees/profile/form',             [EmployerController::class, 'update']);
Route::post('/employees/profile/form',            [EmployerController::class, 'insert']);

Route::get('/category/profile',                   [CategoryController::class, 'viewCategoryProfile']);
Route::get('/category/{id}/showproducts',         [CategoryController::class, 'showProducts']);
Route::get('/category/profile/delete/{id}',       [CategoryController::class, 'destroy']);
Route::post('/category/form',                     [CategoryController::class, 'insert']);
Route::put('/category/form',                      [CategoryController::class, 'update']);
Route::get('/category/create',                    [CategoryController::class, 'create']);
Route::get('/category/{id}/edit',                 [CategoryController::class, 'edit']);

Route::get('/product/profile',                    [ProductController::class,  'viewProducts']);
Route::get('/product/profile/delete/{id}',        [ProductController::class,  'destroy']);
Route::get('/product/edit/{id}',                  [ProductController::class,  'edit']);
Route::get('/product/create',                     [ProductController::class,  'create']);
Route::put('/product/form',                       [ProductController::class,  'update']);
Route::post('/product/form',                      [ProductController::class,  'insert']);

Route::get('/inventory/profile',                  [InventoryController::class, 'viewInventory']);
Route::get('/inventory/create',                   [InventoryController::class, 'create']);
Route::get('/inventory/delete/{id}',              [InventoryController::class, 'destroy']);
Route::put('/inventory/form',                     [InventoryController::class, 'update']);
Route::post('/inventory/form',                    [InventoryController::class, 'insert']);

Route::get('/promotion/profile',                  [PromotionController::class, 'viewPromotion']);
Route::get('/promotion/create',                   [PromotionController::class, 'create']);
Route::get('/promotion/delete/{id}',              [PromotionController::class, 'destroy']);
Route::get('/promotion/edit/{id}',                [PromotionController::class, 'edit']);
Route::put('/promotion/form',                     [PromotionController::class, 'update']);
Route::post('/promotion/form',                    [PromotionController::class, 'insert']);

Route::get('/sales/profile',                      [SaleController::class, 'viewSale']);
Route::get('/sales/create',                       [SaleController::class, 'create']);
Route::get('/sales/delete/{id}',                   [SaleController::class, 'destroy']);
Route::put('/sales/form',                         [SaleController::class, 'update']);
Route::post('/sales/form',                        [SaleController::class, 'insert']);
Route::get('/sales/showsale/{id}',                [SaleController::class, 'show']);

