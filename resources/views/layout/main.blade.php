<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
</head>

<body>
    <h1 style="text-align: center">Mercado</h1>
    <header>
        <a href="/"> Home </a>
        <a href="{{ url('/customersprofile') }}"> Clientes </a>
        <a href="{{ url('/employees/profile') }}"> Funcionarios </a>
        <a href="{{url('/inventory/profile')}}"> Estoque </a>
        <a href="{{ url('/sales/profile')}}"> Vendas </a>
        <a href="{{ url('/product/profile')}}"> Produtos</a>
        <a href="{{url('/promotion/profile')}}"> Promoção</a>
        <a href="{{ url('/category/profile') }}"> Categorias</a>
        @yield('content')
</body>
