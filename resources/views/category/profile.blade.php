@extends('layout.main')

@section('title', 'Category')

@section('content')

    <h1 style="text-align: center"> Categorias </h1>

    <a href="{{url('/category/create') }}"> Criar categoria </a>

    <table>
        <thead>
            <tr>
                <th>Id</th>
                <th>Nome</th>
            </tr>
        </thead>

        @foreach ( $categories as $category )

        <tbody>
            <tr>
                <td>{{$category->id}}</td>
                <td>{{$category->name}}</td>
                <td><a href="/category/{{$category->id}}/showproducts" >Mostrar produtos</a></td>
                <td><a href="/category/profile/delete/{{ $category->id }}">Excluir<a></td>
                <td><a href="/category/{{$category->id}}/edit">Editar</a></td>
            </tr>
        </tbody>
        @endforeach
@endsection
