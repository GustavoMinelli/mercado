@extends('layout.main')

@section('title', 'Category')

@section('content')

    <form action="{{url('/category/form')}}" method="POST">

        @csrf

        @method($isEdit ? "PUT" : "POST")
        <br>
        <label>Nome</label>
        <input type="text" required name="name" value="{{$category->name ?? ""}}">

        @if ($isEdit)
            <input type="hidden" name="id" value="{{$category->id}}">

        @endif

        <button type="submit" class="btn btn-primary btn-lg"> Salvar</button>

@endsection
