@extends('layout.main');

@section('title', 'Promoçao')

@section('content')

<h1> Promoção </h1>

    <a href="{{url('/promotion/create')}}">Criar uma promoção</a>

    <table>
        <thead>
            <tr>
                <th>ID</th>
                <th>Preço</th>
                <th>Esta ativo?</th>
                <th>Data de inicio</th>
                <th>Data final</th>
                <th>Açoes</th>
            </tr>
        </thead>

        @foreach ($promotions as $promotion)

        <tbody>
            <tr>
                <td>{{$promotion->id}}</td>
                <td>{{$promotion->price}}</td>
                <td>{{$promotion->is_active ? "Ativo" : "Inativo"}}</td>
                <td>{{$promotion->started_at}}</td>
                <td>{{$promotion->ended_at}}</td>
                <td><a href="/promotion/delete/{{$promotion->id}}">Excluir<a>
                <a href="/promotion/edit/{{$promotion->id}}">Editar<a></td>
            </tr>
        </tbody>

        @endforeach

@endsection
