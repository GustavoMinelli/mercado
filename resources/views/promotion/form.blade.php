    @extends('layout.main');

@section('title', 'Promo')

@section('content')

    <h1>Promoçao</h1>

    <form action="{{ url('promotion/form') }}" method="POST">

        @csrf

        @method($isEdit ? 'PUT' : 'POST')

        <label>Preço</label>
        <input type="number" required name="price" value="{{ $promotion->price ?? '' }}">

        Esta ativo?<input type="checkbox" name="is_active"{{ $promotion->is_active ? 'checked' : '' }} value="true">
        <br>

        <label>Data de inicio</label>
        <input type="date" required name="started_at" value="{{(string) $promotion->started_at ? $promotion->started_at->format('Y-m-d') : ""}}">

        <br>

        <select name="product_id">
            @foreach ($products as $product)
                <option value="{{ $product->id }}">{{ $product->name }}</option>
            @endforeach
        </select>


        <label>Data de fim</label>
        <input type="date" required name="ended_at" value="{{(string) $promotion->ended_at ? $promotion->ended_at->format('Y-m-d') : "" }}">


        @if ($isEdit)

        <input type="hidden" name="id" value="{{$promotion->id}}">

        @endif


        <br>

        <button type="submit"class="btn btn-primary btn-lg">Salvar</button>

    @endsection
