@extends('layout.main')

@section('title', 'employees')

@section('content')
    <br>
    <h1 style="text-align: center">Funcionários</h1>

    <a href="{{'/employees/profile/create'}}">Criar Funcionario</a>

    <table>
        <thead>
            <tr>
                <th>id</th>
                <th>Nome</th>
                <th>Endereço</th>
                <th>rg</th>
                <th>cpf</th>
                <th>email</th>
                <th>phone</th>
                <th>work code</th>
            </tr>
        </thead>
        <tbody>
            @foreach ( $employees as $employee )
                <tr>
                    <td>{{ $employee->id }}</td>
                    <td>{{ $employee->name }}</td>
                    <td>{{ $employee->address }}</td>
                    <td>{{ $employee->rg }}</td>
                    <td>{{ $employee->cpf }}</td>
                    <td>{{ $employee->email }}</td>
                    <td>{{ $employee->phone }}</td>
                    <td>{{ $employee->work_code }}</td>
                    <td><a href="/employees/profile/delete/{{ $employee->id }}">Excluir<a></td>
                    <td><a href="/employees/profile/edit/{{ $employee->id }}">Editar</a></td>
                </tr>
            @endforeach
        </tbody>

    </table>
@endsection
