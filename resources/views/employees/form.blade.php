@extends('layout.main')

@section('title', 'Employee')

@section('content')

    <form action="{{url('/employees/profile/form')}}" method="POST">

        @csrf

        @method($isEdit ? "PUT" : "POST")

        <label>Nome</label>
        <input type="text" required name="name"  value="{{$employee->name ?? ""}}">
        <br>

        <label>Email</label>
        <input type="email" required name="email" value="{{$employee->email ??""}}">
        <br>

        <label>Endereço</label>
        <input type="text" required name="address" value="{{$employee->address ??""}}" >
        <br>

        <label>RG</label>
        <input type="number" required name="rg" value="{{$employee->rg ??""}}">
        <br>

        <label>CPF</label>
        <input type="number" required name="cpf" value="{{$employee->cpf ??""}}">
        <br>

        <label>Telefone</label>
        <input type="number" required name="phone" value="{{$employee->phone ??""}}">
        <br>

        <label>Carteira de trabalho</label>
        <input type="number" required name="work_code" value="{{$employee->work_code ??""}}">
        <br>


        @if ($isEdit)
            <input type="hidden" name="id" value="{{$employee->id}}">
        @endif

        <button type="submit"class="btn btn-primary btn-lg">Salvar</button>

    </form>

@endsection
