@extends('layout.main')

@section('title', 'Customers')

@section('content')
    <h1 style="text-align: center"> Clientes</h1>

    <a href="{{ '/customers/profile/create' }}">Criar Cliente</a>
    <table>
        <thead>
            <tr>
                <th>Id</th>
                <th>Nome</th>
                <th>email</th>
                <th>endereço</th>
                <th>Rg</th>
                <th>Cpf</th>
        </thead>

        @foreach ($customers as $customer)

        <tbody>
                <tr>
                    <td>{{ $customer->id }}</td>
                    <td>{{ $customer->name }}</td>
                    <td>{{ $customer->email }}</td>
                    <td>{{ $customer->address }}</td>
                    <td>{{ $customer->rg }}</td>
                    <td>{{ $customer->cpf }}</td>
                    <td><a href="/customers/profile/edit/delete/{{ $customer->id }}">Excluir<a></td>
                    <td><a href="/customers/profile/edit/{{ $customer->id }}">Editar</a></td>
                </tr>
        </tbody>
        @endforeach
@endsection
