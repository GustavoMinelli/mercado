@extends('layout.main')

@section('title', 'Customers')

@section('content')

    <form action="{{url('/customers/profile/form') }}" method="POST">

        @csrf

        @method($isEdit ? "PUT" : "POST")

        <label>Nome</label>
        <input type="text" required name="name"  value="{{$customer->name ?? ""}}">
        <br>


        <label>Email</label>
        <input type="email" required name="email" value="{{$customer->email ??""}}">
        <br>

        <label>Endereço</label>
        <input type="text" required name="address" value="{{$customer->address ??""}}" >
        <br>

        <label>RG</label>
        <input type="number" required name="rg" value="{{$customer->rg ??""}}">
        <br>

        <label>CPF</label>
        <input type="number" required name="cpf" value="{{$customer->cpf ??""}}">
        <br>

        @if ($isEdit)
            <input type="hidden" name="id" value="{{$customer->id}}">
        @endif

        <button type="submit"class="btn btn-primary btn-lg">Salvar</button>

    </form>

@endsection
