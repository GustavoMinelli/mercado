@extends('layout.main');

@section('title', 'Products')

@section('content')

<h1> Produtos </h1>

<a href="{{url('/product/create')}}"> Criar um produto </a>

<table>
    <thead>
        <tr>
            <th>Id</th>
            <th>Nome</th>
            <th>Preço</th>
            <th>Quantidade</th>
        </tr>
    </thead>

    @foreach ($products as $product)

    <tbody>
        <tr>
            <td>{{$product->id}}</td>
            <td>{{$product->name}}</td>
            <td>{{$product->price}}</td>
            <td>{{$product->qty}}</td>
            <td><a href="/product/profile/delete/{{$product->id}}">Excluir<a></td>
            <td><a href="/product/edit/{{$product->id}}">Editar<a></td>
            </tr>
    </tbody>
    @endforeach
@endsection


