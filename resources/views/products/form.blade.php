@extends('layout.main');

@section('title', 'Products')

@section('content')

    <form action="{{url('/product/form')}}" method="POST">

        @csrf

        @method($isEdit ? "PUT" : "POST")

        <label>Nome</label>
        <input type="text" required name="name"  value="{{$product->name ?? ""}}">
        <br>

        <select name="category_id">
            @foreach ($categories as $category )
                <option value="{{$category->id}}">{{$category->name}}</option>
            @endforeach
        </select>
        <br>

        <label>Preço</label>
        <input type="number" step="0.01" required name="price" value="{{$product->price ??""}}">
        <br>

        @if ($isEdit)
            <input type="hidden" name="id" value="{{$product->id}}">
        @endif

        <button type="submit"class="btn btn-primary btn-lg">Salvar</button>
    </form>

@endsection
