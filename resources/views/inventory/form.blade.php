@extends('layout.main');

@section('title', 'Estoque')

@section('content')

<h1>Estoque</h1>

<form action="{{url('/inventory/form')}}" method="POST">

    @csrf

    @method($isEdit ? "PUT" : "POST")

    <select name="product_id">
        @foreach ($products as $product )
            <option value="{{$product->id}}">{{$product->name}}</option>
        @endforeach
    </select>
    <br>
    <label>Quantidade</label>
    <input type="number" required name="qty" value="{{$inventory->qty ??""}}">
    <br>

    <label>Data de inicio</label>
    <input type="date" required name="created_at" value="{{$inventory->created_at ??""}}">
    <br>

    <button type="submit"class="btn btn-primary btn-lg">Salvar</button>
@endsection
