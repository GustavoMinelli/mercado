@extends('layout.main');

@section('title', 'Estoque')

@section('content')

<h1> Estoque </h1>

<a href="/inventory/create"> Criar um estoque </a>

<table>
    <thead>
        <tr>
            <th> id </th>
            <th>Produto</th>
            <th>Quantidade</th>
        </tr>
    </thead>

    @foreach ( $inventories as $inventory )

    <tbody>
        <tr>
            <td>{{$inventory->id}}</td>
            <td>{{$inventory->product_id}}</td>
            <td>{{$inventory->qty}}</td>
            <td><a href="{{url('/inventory/delete/'.$inventory->id)}}">Deletar</a>
        </tr>
    </tbody>
    @endforeach
@endsection
