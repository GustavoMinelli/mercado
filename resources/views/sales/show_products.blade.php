@extends('layout.main')

@section('title', 'Vendas')

@section('content')

    <h1> Produtos da venda </h1>

    <a href="/sales/profile"> Voltar para vendas</a>
    <table>

        <thead>

            <tr>

                <th>ID</th>
                <th>Cliente</th>
                <th>Funcionarios</th>
                <th>Produto</th>
                <th>Quantidade</th>
                <th>Valor</th>

            </tr>

        </thead>
        @foreach ($sales as $sale)

        <tbody>

            <tr>

                <th>{{$sale->id}}</th>
                <th>{{$sale->client}}</th>
                <th>{{$sale->employee}}</th>
                <th>{{$sale->product}}</th>
                <th>{{$sale->qty_sales}}</th>
                <th>{{$sale->total_price}}</th>

            </tr>

        </tbody>
        @endforeach
    </table>
@endsection
