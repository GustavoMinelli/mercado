@extends('layout.main')

@section('title', 'Vendas')

@section('content')

    <h1>vendas</h1>

    <form action="{{url('/sales/form')}}" method="POST">

            @csrf

            <br>

            <label>Cliente</label>
            <select name="customer_id" required>
                <option selected>Selecione um cliente</option>

                @foreach ($customers as $customer)
                    <option value="{{$customer->id}}">{{$customer->name}}</option>
                @endforeach

            </select>

            <label>Funcionario</label>
            <select name="employee_id" required>
                <option selected>Selecione um funcionario</option>

                 @foreach ($employees as $employee)

                    <option value="{{$employee->id}}">{{$employee->name}}</option>

                @endforeach
            </select>
            <br>

            @if (count($products) > 0)

            <label>Qual produto e qual sua quatidade?</label>
            @foreach ($products as $k => $product)
                <input type="checkbox" name="product_id[{{$k}}]" value="{{ $product->id }}">{{$product->name}}

                <input type="number" name="qty_sales[]">
                <br>



            @endforeach


            @else
                <h2> nenhuma venda criada</h2>




        <br>
            @endif
        <button type="submit"class="btn btn-primary btn-lg">Salvar</button>
@endsection
