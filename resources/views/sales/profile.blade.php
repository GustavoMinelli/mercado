@extends('layout.main')

@section('title', 'Vendas')

@section('content')

    <h1>Vendas</h1>

    <a href="/sales/create"> Criar venda</a>
    <table>

        <thead>

            <tr>

                <th>ID</th>
                <th>Valor</th>
                <th>Açoes</th>

            </tr>

        </thead>

        @foreach ($sales as $sale)

        <tbody>

            <tr>

                <td>{{$sale->id}}</td>
                <td>{{$sale->total}}</td>
                <td><a href="{{url("/sales/showsale/".$sale->id)}}">Visualizar</a>
                    <a href="{{url("/sales/delete/".$sale->id)}}">Deletar</a>
                </td>


            </tr>

        </tbody>
        @endforeach
    </table>
@endsection
