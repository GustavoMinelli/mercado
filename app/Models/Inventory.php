<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    use HasFactory;
    protected $table = 'inventory';
    protected $fillable = [
        'product_id',
        'created_at',
        'ended_at',
        'qty',
    ];
    public $timestamps = false;

    public function product()
    {
        return $this->belongsTo(Product::class,);
    }
}
