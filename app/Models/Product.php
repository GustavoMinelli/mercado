<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $table = 'products';
    protected $fillable = [
        'name',
        'price',
        'current_qty',
    ];
    public function category(){

        return $this->belongsTo(Category::class);
    }
    public function inventory(){

        return $this->hasMany(Inventory::class);
    }
    public function promotion(){

        return $this->belongsTo(Promotion::class);
    }


}
