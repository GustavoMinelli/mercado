<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Promotion;
use Illuminate\Http\Request;

class PromotionController extends Controller
{
    public function viewPromotion(){

        $promotions = Promotion::get();
        $data = [
            'promotions' => $promotions
        ];


        return view('promotion.profile', $data);
    }

    public function create(){

        return $this->form(new Promotion);
    }

    public function save(Promotion $promotion, Request $request){

        $promotion->product_id = $request->product_id;
        $promotion->price = $request->price;
        $promotion->started_at = $request->started_at;
        $promotion->ended_at = $request->ended_at;
        $promotion->is_active = $request->is_active;

        $promotion->save();
    }
    public function form(Promotion $promotion){


        $isEdit = $promotion->id ? true : false;

        $products = Product::get();

        $data=[
            'promotion'=>$promotion,
            'isEdit'=>$isEdit,
            'products'=>$products,
        ];

        return view('promotion.form', $data);
    }

    public function edit(Request $request){

        $promotion=Promotion::find($request->id);

        return $this->form($promotion);
    }

    public function destroy($id){

        Promotion::findOrFail($id)->delete();

        return redirect('/promotion/profile');

    }
    public function insert(Request $request){

        $promotion = new Promotion();

        $this->save($promotion, $request);

        return redirect('/promotion/profile');
    }
    public function update(Request $request){

        $promotion = Promotion::find($request->id);

        $this->save($promotion, $request);

        return redirect('/promotion/profile');
    }

}

