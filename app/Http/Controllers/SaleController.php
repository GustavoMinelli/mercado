<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Employee;
use App\Models\Product;
use App\Models\ProductsSale;
use App\Models\Promotion;
use App\Models\Sale;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Exception;
use Illuminate\Support\Facades\DB;

class SaleController extends Controller
{
    public function viewSale()
    {

        $sales = Sale::get();
        $data = [
            'sales' => $sales
        ];

        return view('sales.profile', $data);
    }

    public function create()
    {

        return $this->form(new Sale);
    }

    public function destroy($id)
    {

        $sale = Sale::find($id);

        $qty = Sale::searchQty($id)->get();
        foreach ($qty as $productQty) {

            $product = Product::find($productQty->product_id);

            $product->increment('qty', $productQty->qty_sales);
        }
        $sale->delete();

        return redirect('/sales/profile');
    }

    public function show($id)
    {

        $sales = Sale::search($id)->get();

        $data = [
            'sales' => $sales
        ];

        return view('sales.show_products', $data);
    }

    public function form(Sale $sale)
    {

        $isEdit = $sale->id ? true : false;

        $products = Product::get();

        $employees = Employee::get();

        $customers = Customer::get();


        $data = [
            'products' => $products,
            'isEdit' => $isEdit,
            'employees' => $employees,
            'customers' => $customers,
        ];

        return view('sales.form', $data);
    }
    public function insert(Request $request)
    {
        $sale = new Sale();
        $this->save($sale, $request);
        return redirect('/sales/profile');
    }
    public function update(Request $request)
    {

        $sale = Sale::find($request->id);
        $this->save($sale, $request);
        return redirect('/sales/profile');
    }

    private function save(Sale $sale, Request $request)
    {
        // dd($request->all());


        try{

            DB::beginTransaction();

            $sale->customer_id = $request->customer_id;
            $sale->employee_id = $request->employee_id;

            $products = $request->product_id;
            $sale->save();


            foreach ($products as $k => $product){

                $product = Product::find($product);

                $price = Promotion::searchPrice($product)->first();

                // dd($price);

                if($request->qty_sales[$k]){


                    $qty_sale = (int)$request->qty_sales[$k];

                    if(isset($price->is_active)){
                        $total_price = $qty_sale * $price->promotion;

                    }
                    else{
                        $total_price = $qty_sale * $price->product;
                    }

                $AttachArray = [
                'qty_sales' => $qty_sale,
                'total_price' => $total_price
                ];
            }

            $sale->products()->attach($product, $AttachArray);

            $product->decrement('qty', $qty_sale);

            $sale->increment('total', $total_price);
            }


            DB::commit();
        } catch (Exception $e) {

            dd($e->getMessage());
            DB::rollBack();
        }
    }
}
