<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;

class EmployerController extends Controller
{

    public function viewEmployerProfile()
    {
        $employees = Employee::get();
        $data = [
            'employees' => $employees
        ];
        return view('employees.profile', $data);
    }

    public function create()
    {
        return $this->form(new Employee());
    }

    public function destroy($id)
    {
        Employee::find(($id))->delete();

        return redirect('/employees/profile');
    }

    public function save(Employee $employee, Request $request)
    {
        $employee->name = $request->name;
        $employee->email = $request->email;
        $employee->address = $request->address;
        $employee->phone = $request->phone;
        $employee->work_code = $request->work_code;
        $employee->cpf = $request->cpf;
        $employee->rg = $request->rg;

        $employee->save();
    }

    public function form(Employee $employee)
    {
        $isEdit = $employee->id ? true : false;
        $data = [
            'employee' => $employee,
            'isEdit' => $isEdit
        ];
        return view('employees.form', $data);
    }

    public function insert(Request $request)
    {
        $employee = new Employee();
        $this->save($employee, $request);
        return redirect('/employees/profile');
    }

    public function update(Request $request)
    {
        $employee = Employee::find($request->id);
        $this->save($employee, $request);
        return redirect('/employees/profile');
    }

    public function edit(Request $request)
    {

        $employee = Employee::find($request->id);
        return $this->form($employee);
    }
}
