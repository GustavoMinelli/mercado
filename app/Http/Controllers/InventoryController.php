<?php

namespace App\Http\Controllers;

use App\Models\Inventory;
use App\Models\Product;
use Illuminate\Http\Request;

class InventoryController extends Controller
{
    public function viewInventory(){

        $inventories = Inventory::get();
        $data=[
            'inventories'=>$inventories
        ];


        return view('inventory.profile', $data);
    }

    public function create(){

        return $this->form(new Inventory());
    }

    public function save(Inventory $inventory, Request $request){

        $product = Product::find($request->product_id);

        $product->increment('qty', $request->qty);

        $inventory->product_id = $request -> product_id;

        $inventory->qty = $request->qty;

        $inventory->save();
    }

    public function form(Inventory $inventory){

        $isEdit = $inventory->id ? true:false;
        $products = Product::get();
        $data=[
            'inventory'=>$inventory,
            'isEdit'=>$isEdit,
            'products'=>$products,
        ];
        return view('inventory.form', $data);
    }

    public function edit(Request $request){

        $inventory=Inventory::find($request->id);
        return $this->form($inventory);
    }

    public function destroy($id){

        Inventory::find($id)->delete();

        return redirect('/inventory/profile');
    }
    public function insert(Request $request){

        $inventory = new Inventory();
        $this->save($inventory, $request);
        return redirect('/inventory/profile');
    }
    public function update(Request $request){

        $inventory = Inventory::find($request->id);
        $this->save($inventory, $request);
        return redirect('/inventory/profile');

    }
}
