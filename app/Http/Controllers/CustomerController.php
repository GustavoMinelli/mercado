<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;
use PhpParser\Node\Expr\New_;

class CustomerController extends Controller
{

    public function create()
    {

        return $this->form(new Customer());
    }

    public function viewCostumerCreateProfile()
    {

        return view('customer.profile');
    }

    public function save(Customer $customer, Request $request)
    {


        $customer->name = $request->name;
        $customer->email = $request->email;
        $customer->address = $request->address;
        $customer->rg = $request->rg;
        $customer->cpf = $request->cpf;

        $customer->save();
    }

    public function viewCustomerProfile()
    {

        $customers = Customer::get();
        $data = [
            'customers' => $customers,
        ];
        return view('customer.profile', $data);
    }

    public function destroy($id)
    {

        Customer::find(($id))->delete();

        return redirect('/customersprofile');
    }
    public function edit(Request $request)
    {

        $customer = Customer::find($request->id);
        return $this->form($customer);
    }

    public function form(Customer $customer)
    {
        $isEdit = $customer->id ? true : false;
        $data = [
            'customer' => $customer,
            'isEdit' => $isEdit
        ];
        return view('customer.form', $data);
    }

    public function insert(Request $request)
    {
        $customer = new Customer();
        $this->save($customer, $request);
        return redirect('/customersprofile');
    }

    public function update(Request $request)
    {
        $customer = Customer::find($request->id);
        $this->save($customer, $request);
        return redirect('/customersprofile');
    }
}
