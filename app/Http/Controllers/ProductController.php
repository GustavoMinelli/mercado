<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function viewProducts(){

        $products = Product::get();
        $data=[
                'products'=>$products,
        ];


       return view('products.profile', $data);
    }

    public function create()
    {

    return $this->form(new Product());
    }
    public function save(Product $product, Request $request)
    {

        $product->name = $request->name;
        $product->price = $request->price;

        $product->category_id = $request->category_id;

        $product->save();
    }

    public function form(Product $product)
    {
        $isEdit = $product->id ? true : false;
        $categories = Category::get();
        $data = [
            'product' => $product,
            'isEdit' => $isEdit,
            'categories' =>$categories,
        ];
        return view('products.form', $data);
    }
    public function edit(Request $request)
    {

        $product = Product::find($request->id);
        return $this->form($product);
    }

    public function destroy($id)
    {

        Product::find(($id))->delete();

        return redirect('/product/profile');

    }
    public function insert(Request $request)
    {

        $product = new Product();
        $this->save($product, $request);
        return redirect('/product/profile');
    }
    public function update(Request $request){

        $product = Product::find($request->id);
        $this->save($product, $request);
        return redirect('/product/profile');
    }

}
