<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    public function create()
    {

        return $this->form(new Category ());

    }

    public function viewCategoryProfile()
    {
        $categories = Category::get();
        $data=[
            'categories'=> $categories,

        ];
        return view('category.profile', $data);
    }

    public function save(Category $category, Request $request){

        $category->name = $request->name;

        $category->save();

    }

    public function destroy($id){

        Category::find(($id))->delete();

        return redirect('/category/profile');

    }

    public function form(Category $category){
        $isEdit = $category->id ? true : false;
        $data = [
            'category' => $category,
            'isEdit' => $isEdit
        ];
        return view('category.form', $data);
    }

    public function insert(Request $request){

        $category = new Category();
        $this -> save($category, $request);

        return redirect('/category/profile');
    }

    public function edit(Request $request)
    {


        $category = Category::find($request->id);
        return $this->form($category);

    }
    public function update(Request $request)
    {

        $category = Category::find($request->id);
        $this->save($category, $request);
        return redirect('/category/profile');
    }

    public function showProducts($id)
    {
        $category = Category::find($id);
        $data = [
            'category' => $category
        ];

        return view('category.show_products', $data);
    }

}
